<?php

// Алгоритм поиск в ширину
function bfsShortestPath($graph, $startNode, $endNode) {
    // Инициализируем очередь для обхода графа
    $queue = new SplQueue();
    $queue->enqueue([$startNode]);

    // Проверяем начальный узел, если он совпадает с конечным, возвращаем пустой путь
    if ($startNode === $endNode) {
        return [];
    }

    // Создаем массив для хранения посещенных узлов
    $visited = [];

    // Итеративно обходим граф
    while (!$queue->isEmpty()) {
        $path = $queue->dequeue();
        $node = $path[count($path) - 1];

        // Проверяем, является ли текущий узел конечным, если да, возвращаем найденный путь
        if ($node === $endNode) {
            return $path;
        }

        // Проверяем всех соседей текущего узла
        foreach ($graph[$node] as $neighbor) {
            if (!isset($visited[$neighbor])) {
                // Если соседний узел еще не посещен, добавляем его в очередь
                $newPath = $path;
                $newPath[] = $neighbor;
                $queue->enqueue($newPath);

                $visited[$neighbor] = true;
            }
        }
    }

    // Если путь не найден, возвращаем null
    return null;
}

?>
