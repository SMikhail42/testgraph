<?php

// Волновой алгоритм
function waveAlgorithm($graph, $startNode, $endNode) {
    // Инициализируем очередь для обхода графа
    $queue = new SplQueue();
    $queue->enqueue($startNode);

    // Создаем массив для хранения посещенных узлов
    $visited = [];

    // Создаем массив для хранения расстояния до каждого узла
    $distances = [];
    foreach ($graph as $node => $neighbors) {
        $distances[$node] = PHP_INT_MAX; // Изначально устанавливаем расстояние до каждого узла как бесконечность
    }
    $distances[$startNode] = 0; // Расстояние до начального узла устанавливаем как 0

    // Итеративно обходим граф
    while (!$queue->isEmpty()) {
        $currentNode = $queue->dequeue();

        // Если мы достигли конечного узла, прерываем цикл
        if ($currentNode === $endNode) {
            break;
        }

        // Проверяем всех соседей текущего узла
        foreach ($graph[$currentNode] as $neighbor) {
            if (!isset($visited[$neighbor])) {
                // Если соседний узел еще не посещен, добавляем его в очередь
                $queue->enqueue($neighbor);
                $visited[$neighbor] = true;

                // Обновляем расстояние до соседнего узла, если нашли более короткий путь
                $distances[$neighbor] = $distances[$currentNode] + 1;
            }
        }
    }

    // Возвращаем кратчайшее расстояние до конечного узла
    return $distances[$endNode];
}

?>
